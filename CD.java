/**
 * This class implements CD products. They have an author and can play music.
 * @author Ventsislav Yordanov
 */
public class CD extends Playable {
	protected String artist;
	
	/**
	 * Create a new CD playable product.
	 * @param price The price of the product in pounds.
	 * @param numStock The amount of the product in stock. 
	 * @param runTime The runtime of the product.
	 * @param title The title of the product.
	 * @param artist The name of the artist.
	 */
	public CD(double price, int numStock, double runtime, String title, String artist){
		super(price, numStock, runtime, title);
		this.artist = artist;
	}
	
	/**
	 * Set the name of the artist.
	 * @param artist The name of the artist.
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	/**
	 * Get the name of the artist.
	 * @return The name of the artist.
	 */
	public String getArtist() {
		return artist;
	}
	
	/**
	 * Enter play mode on the CD object.
	 */
	@Override
	public void play() {
		System.out.printf("%s CD is playing music. \n", this.title);
	}
}
