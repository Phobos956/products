/**
 * This class implements Teacup products. Which have tea volume they can hold.
 * @author Ventsislav Yordanov
 */
public class Teacup extends Product {	
	protected int volumeOfTea;
	
	/**
	 * Create a new teacup product.
	 * @param price The price of the teacup in pounds.
	 * @param numStock The amount of the teacup in stock.
	 */
	public Teacup (double price, int numStock) {
		super(price, numStock);
	}
	
	/**
	 * Set the volume of tea the cup can hold in ml.
	 * @param volumeOfTea Volume of tea the cup can hold in ml.
	 */
	public void setVolumeOfTea(int volumeOfTea) {
		this.volumeOfTea = volumeOfTea;
	}
	
	/**
	 * Get the volume of tea the cup holds in ml.
	 * @return The volume of tea the cup holds in ml.
	 */
	public int getVolumeOfTea()	{
		return volumeOfTea;
	}
}
