/**
 * This class implements DVD products. They have a director and can play a movie.
 * @author Ventsislav Yordanov
 */
public class DVD extends Playable {
	protected String director;
	
	/**
	 * Create a new DVD playable product.
	 * @param price The price of the DVD in pounds.
	 * @param numStock The amount of the DVD in stock. 
	 * @param runTime The runtime of the DVD.
	 * @param title The title of the DVD.
	 * @param director The name of the director.
	 */
	public DVD(double price, int numStock, double runtime, String title, String director) {
		super(price, numStock, runtime, title);
		this.director = director;
		this.rentalCost = 1.20;
	}
	
	/**
	 * Set the name of the director.
	 * @param director The name of the director.
	 */
	public void setDirector(String director) {
		this.director = director;
	}
	
	/**
	 * Get the name of the director.
	 * @return The name of the director.
	 */
	public String getDirector() {
		return director;
	}
	
	/*
	 * Enter play mode on the DVD object.
	 */
	@Override
	public void play() {
		System.out.printf("%s DVD is playing a movie. \n", this.title);
	}
}
